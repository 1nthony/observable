#!/bin/bash

#SBATCH --job-name=XTAL_test
#SBATCH --partition=cpu_long

#SBATCH --time=01:00:00 
#SBATCH --ntasks=1 
##SBATCH --cpus-per-task=1
#SBATCH --exclusive
#SBATCH --mail-user=anthony.benois@student-cs.fr
#SBATCH --mail-type=ALL
#SBATCH --mem=10GB
# Load the same modules as environment configuration

module purge 
module load intel-parallel-studio/cluster.2020.2/intel-20.0.2

CRYSTAL_BIN_PATH=/gpfs/softs/softwares/crystal14/1.0.4/bin

CRYSTAL_OUT_PATH=/gpfs/workdir/benoisa/CRYSTAL/CO/rdm

#######  SCF COMPUTATION ############

##INPUT_CRYSTAL=/gpfs/workdir/benoisa/CRYSTAL/CO/scf/Input_RDM_CO.d3

##ln -sf $INPUT_CRYSTAL INPUT

##id
##srun $CRYSTAL_BIN_PATH/Pcrystal 2> $CRYSTAL_OUT_PATH/out.out

#######  PROPERTIES COMPUTATION ############

##INPUT_PROPERTIES=OUT.d3 
INPUT_PROPERTIES=/gpfs/workdir/benoisa/CRYSTAL/CO/scf/Input_RDM_CO.d3

ln -sf $INPUT_PROPERTIES INPUT
srun $CRYSTAL_BIN_PATH/Pproperties 2> $CRYSTAL_OUT_PATH/propOUT.out

##rm -f KRED.DAT
##$CRYSTAL_OUT_PATH/Cry_out.exe

