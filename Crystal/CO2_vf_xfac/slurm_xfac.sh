#!/bin/bash

#SBATCH --job-name=XTAL_test
#SBATCH --partition=cpu_short

#SBATCH --time=00:10:00 
#SBATCH --ntasks=80 
##SBATCH --cpus-per-task=80
#SBATCH --exclusive
#SBATCH --mail-user=anthony.benois@student-cs.fr
#SBATCH --mail-type=ALL
#SBATCH --mem=40GB
# Load the same modules as environment configuration

module purge 
module load intel-parallel-studio/cluster.2020.2/intel-20.0.2

CRYSTAL_BIN_PATH=/gpfs/softs/softwares/crystal14/1.0.4/bin

CRYSTAL_OUT_PATH=/gpfs/workdir/benoisa/CRYSTAL/CO2_vf_xfac
	
#######  SCF COMPUTATION ############

INPUT_CRYSTAL=/gpfs/workdir/benoisa/CRYSTAL/CO2_vf_xfac/CO2_input_xfac.d12

ln -sf $INPUT_CRYSTAL INPUT

id
srun $CRYSTAL_BIN_PATH/Pcrystal 2> $CRYSTAL_OUT_PATH/out_xfac.out

#######  PROPERTIES COMPUTATION ############

##INPUT_PROPERTIES=OUT.d3 
##INPUT_PROPERTIES=Input_XFACprop.d12

##ln -sf $INPUT_PROPERTIES INPUT
##srun $CRYSTAL_BIN_PATH/Pproperties 2> $CRYSTAL_OUT_PATH/propOUT.out

##rm -f KRED.DAT
##$CRYSTAL_OUT_PATH/Cry_out.exe

