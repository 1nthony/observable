####################################
Sommaire
####################################

- Types de fichiers
- Dossiers propres (voir explications dossiers pour les détails)
- Explications dossiers
- Explications Multiple_Inputs
- Procédure lancement calcul
- A faire



####################################
Types de fichiers
####################################

# fort.25
C'est l'objectif ! C'est la RDM en version tableau de nombres, obtenu après calculs adp et scf

# fort...
Fichiers intermédiaires, lourds. Contiennent les fonctions d'onde. 
A ignorer, mais à supprimer avant d'upload sur le git.

# .d12 ou .d3
Fichiers input CRYSTAL. Peut être adp, scf, rdm.

# slurm_?.sh
On appelle ça un script shell.
Fichiers qui lance un "job". Dedans, le langage de programmation est shell.
? peut être adp, scf, rdm, xfac, ou autre (voir dans Multiple_Output)
Concrètement, on spécifie dans ce type de fichier :
- les paramètres du calcul parallèle
- du code à exécuter : dans notre cas, c'est souvent lancer le logiciel CRYSTAL
- le fichier input à utiliser si on lance CRYSTAL
- le fichier output à créer si on lance CRYSTAL
Voir README_SLURM.txt pour les détails
En général, slurm_adp lance les calculs d'adp,
slurme_scf lance les calculs scf, etc...
	(les fichiers initialement donnés par jmg ne s'appelaient pas comme ça,
	donc dans certains dossiers leur appelation peut être un peu différente) 

# slurm-JOB.out
C'est la réponse au script shell (slurm) lancé, dont le numéro de job est JOB

# .DAT
Fichiers de "rapports" intermédiaires. 
Ca peut être des données sur l'optimisation, les fréquences, les forces etc



####################################
Dossiers propres (voir explications dossiers pour les détails)
####################################

- CO_v1
- CO_v1-1
- CO_v2 (mais maille hexagonale : ne fonctionne pas)
- CO2_vf
- CO2_vf_xfac (mais ne marche pas)
- Multiple_Inputs



####################################
Explications dossiers
####################################

# Multiple_Inputs
Coeur du travail accompli. 
Son fonctionnement est détaillé dans la section qui lui est dédié.
Permet de lancer les calculs d'adp, de scf, de rdm, et ce, pour un nombre quelconque de configurations données en entrée.

# clean.py
fichier qui supprime les fichiers fort intermédiaires, en prenant soin de ne 
pas supprimer fort25. Ce fichier a été fait rapidement, donc pour le lancer, 
on est obligé de l'ouvrir et de spécifier le chemin dans lequel on veut 
supprimer les fichiers fort (c'est aussi pour ne pas faire de bêtise...)

# CO
cfc de mémoire (vérifier le groupe d'espace)
Les fichiers sont classés dans dans sous-dossiers adp, scf, rdm
Calcul de la RDM (situé dans le dossier scf) dans les conditions suivantes :
T = 100K
groupe d'espace : 198
a = 5.63 nm

# CO_v1
cfc de mémoire (vérifier le groupe d'espace)
IDENTIQUE A CO, sauf que les fichiers ne sont pas classés dans les sous-dossiers
Calcul de la RDM (situé dans le dossier scf) dans les conditions suivantes :
T = 100K
groupe d'espace : 198
a = 5.63 nm


# CO_v1-1
cfc de mémoire (vérifier le groupe d'espace)
Les fichiers ne sont pas classés dans les sous-dossiers
Calcul de la RDM (situé dans le dossier scf) dans les conditions suivantes :
T = 100K
groupe d'espace : 198
a = 5.575 nm

# CO_v2
Fameuse maille hexagonale de mémoire -> ne marche pas
Les fichiers ne sont pas classés dans les sous-dossiers
Calcul de la RDM (situé dans le dossier scf) dans les conditions suivantes :
P = 2.7 GPa
T = 100K
groupe d'espace : 173
a = 3.615 nm
b = 5.880 nm
(valeurs proviennent de la doc, voir README_DOC.txt)
>> RQ : dans ce dossier, il y a un sous-dossier CO_v2 qui contient un test

# CO2
Contient les fichiers originels
Ce dossier est le dossier avec lequel j'ai pris crystal en main, et avec
lequel j'appris à générer toute la chaîne de calcul (d'où le sous-dossier
test_chaine)

# CO2_vf
pseudo-cfc
Mise au propre du fichier CO2, après maîtrise de la chaîne
C'est à partir de ce moment là que les dossiers commencent à être propres, 
avec des noms de slurm dignes de ce nom.
Calcul de la RDM (situé dans le dossier scf) dans les conditions suivantes :
T = 100K
groupe d'espace : 205
a = 5.63 nm

# CO2_vf_xfac
copie de CO2_vf, avec en plus la tentative d'ajout de calcul de facteurs 
de structure (voir input et slurm concernés)
Le calcul de la rdm marche, mais pas celui des xfac (=facteurs de structure)


####################################
Explications Multiple_Inputs
####################################

/!\ Attention /!\
Penser à changer les chemins absolus, sinon ça marchera pas
Penser à changer l'adresse mail qui recevra les résultats des job (spam non voulu)

# Fort25
Dossier qui contient tous les résultats des calculs de rdm, pour les 
configurations voulues

# Inputs
Dossier dans lequel on va mettre les dossiers input (générés sur mathematica).
A classer dans adp, scf, rdm, xfac
On peut mettre autant de fichiers différents qu'on veut, il faut juste faire
attention à respecter la convention pour les noms de fichiers (le numéro
est particulièrement important, pour identifier les entrées et leur résultat 
associé en sortie
Rq : Mathematica ne génère pour l'instant que les fichiers associés aux adp et scf.
Il faut donc générer nous même l'ensemble des fichiers input_rdm associés
Heureusement, le contenu des input_rdm est identique pour toutes les config,
donc un simple programme python qui recopie le fichier, en donnant le nom qui va
bien (voir slurm_rdm) suffit pour avoir l'ensemble des entrées voulues. Ou alors
demander à la team mathematica de le faire en même temps que le reste des fichiers.

# Output
Contient tous les résultats de calcul :
- un fichier adp_out avec les résultats des calculs d'adp
- un fichier par configuration, dans lequel il y a tous les fichiers mélangés
	(fort...)

# liste_fichiers_input.txt
fichier inutile. dedans sont écrits tous les noms des fichiers d'input

# mk_dir.py
fichier python qui va créer les dossiers dans Output, correspondant 
aux fichiers d'entrée donnés dans Inputs

# slurm_adp.sh
script shell qui lance les calculs d'adp, résultat stockés dans Output/adp_out,
inputs stockés dans Inputs/adp

# slurm_all_config.sh
script shell qui lance successivement :
- slurm_scf_config.sh
- slurm_rdm_config.sh
- slurm_get_fort25.sh
Dans la pratique : on obtient les adp, puis la team mathematica obtient les configurations,
puis on lance script_all_config.sh pour obtenir d'un coup tous les fichiers fort25 dans le 
dossier Fort25. Normalement il marche !

# slurm_get_fort25.sh
script shell qui va "chercher" les fichiers fort25 (=RDM sous la forme de tableaux 
de nombres), et qui les mets dans le dossier Fort25 (en réalité, une simple copie 
est faite, on ne les déplace pas)

# slurm_mk_dir.sh
script shell qui exécute mk_dir.py

# slurm_rdm_config.sh
script shell qui lance les calculs rdm, résultat stockés 
dans tous les dossiers Output/configuration_i (crées par mk_dir.py,
en fonction du nombre d'entrée donnés et leur nom), inputs stockés dans
Inputs/rdm

# slurm_scf_config.sh
script shell qui lance les calculs scf, résultat stockés 
dans tous les dossiers Output/configuration_i (crées par mk_dir.py,
en fonction du nombre d'entrée donnés et leur nom), inputs stockés dans
Inputs/scf


####################################
Procédure lancement calcul
####################################

### Première partie : calculs pour plusieurs configurations ###

---------- REPERTOIRE DE TRAVAIL : MULIPLE_INPUT ----------

# démarche :
- on décide du cristal à étudier
- la team mathematica récupère les positions d'équilibre, et créé
un fichier d'entrée pour crystal
- Placer le fichier d'entrée dans Inputs/adp
1- on lance le calcul des adp
- la team mathematica créé les configurations découlant des adp, 
à placer dans Inputs/scf et Inputs/rdm
2- en fonction du nombre de config créées, on prépare l'arrivée des 
output en créant les dossiers qui accueilleront les résultats
3- pour chaque config, on calcule les fonctions d'onde (scf)
4- pour chaque config, on calcule les matrices densité (rdm)
5- on récupère et rassemble toutes les rdm calculées au même endroit
- on télécharge le dossier Fort25
- on lance le script mathematica que j'ai perdu, qui trace les rdm

# en termes de slurms à lancer :
-
-
-
1- sbatch slurm_adp.sh
- 
2- sbatch slurm_mk_dir.sh
3- sbatch slurm_scf_config.sh
4- sbatch slurm_rdm_config.sh
5- sbatch slurm_get_fort25.sh
-
-

# une autre manière :
-
-
-
1- sbatch slurm_adp.sh
- 
2- sbatch slurm_mk_dir.sh
3-4-5- sbatch slurm_all_config.sh
-
-


### Deuxième partie : calculs pour une seule configuration ###

---------- REPERTOIRE DE TRAVAIL : DOSSIER SPECIFIQUE A L'ESPECE ----------
exemples : les dossiers propres comme CO2_vf, ...
Penser à mettre à jour les dossiers dans les slurms pour que ça marche

La démarche est globalement la même, sauf qu'on a plus à s'embêter à créer 
les différents dossiers.

# démarche :
- on décide du cristal à étudier
- la team mathematica récupère les positions d'équilibre, et créé
un fichier d'entrée pour crystal
1- on lance le calcul des adp
2- on choisit une config (en général la position d'équilibre), et 
on calcule les fonctions d'onde (scf)
3- on calcule la matrice densité (rdm)
- on télécharge la matrice densité (fort25)
- on trace la rdm

# en termes de slurms
-
-
1- sbatch slurm_adp.sh
2- sbatch slurm_scf.sh
3- sbatch slurm_rdm.sh



####################################
A faire
####################################
Voir avec jmg pour les détails.
Voir en particulier avec jmg pour obtenir un premier fichier input_xfac qui marche !

L'idée est d'arriver à faire marcher les calculs de structure de CRYSTAL.
Sauf que un facteur de structure = un triplet d'entiers hkl : il faut savoir 
quels facteurs de structure lui faire calculer. Pour ça, il faut faire une étude
préliminaire, en calculant plein de facteurs de strucure pour différentes températures,
pour trouver ceux qui sont susceptibles de fortement évoluer avec la température.

Pour ça, ça peut être bien de tracer les xfac pour plusieurs températures. 

Une fois qu'on aura trouvé les facteurs qui évoluent pas mal avec la température, 
l'idée est de calculer ces même facteurs de structure pour toutes les configurations
déjà obtenues. Comme ça, la team mathematica pourra moyenner les facteurs de strucure
et valider (ou pas) le projet.

Dans CO2_vf_xfac, il y a déjà un slurm qui s'occupe de lancer les calculs de facteur
de structure (slurm_xfac.sh), il faut juste avoir un fichier input_xfac qui marche 
maintenant, et surtout comprendre pourquoi mon fichier marche pas (demander à jmg)











