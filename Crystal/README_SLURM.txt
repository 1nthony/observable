### Explications des slurms ###
Sommaire :
1- pour une configuration précise (partie simple)
2- slurm_all
3- pour une série de configuration (partie moins simple)
4- Recommendations générales pour programmer en shell



##########################################################
1 : pour une configuration précise (partie simple)
##########################################################
Pour écrire ça, je m'appuie sur CO2_vf_xfac

# partition
Il s'agit d'une estimation grossière du temps de calcul nécessaire
Le supercalculateur s'en sert pour déterminer qui est prioritaire
quand on soumet notre code. Dispo :
- cpu_long
- cpu_short
Le calculateur priorise les gens qui soumettent peu et demandent peu.
cpu_short à privilégier sur les test ou les faibles temps de calculs

# time
Temps maximum d'execution. Si le calcul n'est pas fini au bout de ce temps,
le calcul est abandonné. Le calculateur privilégie des temps courts.

# ntask
En combien de fragments on lance le code en parallèle

# cpus_per_task
Combien d' "ordinateurs" travaillent sur une tâche

# mail
mail qui va recevoir les mails de début et de fin de job (= execution
d'un slurm)

# CRYSTAL_OUT_PATH
chemin du dossier qui reçoit le fichier out

# INPUT_CRYSTAL
nom du fichier input

# ln -sf $INPUT_CRYSTAL INPUT
crystal comprend que le nom du fichier qu'on a déclaré est à considérer comme
une input

# srun $CRYSTAL_BIN_PATH/Pcrystal 2> $CRYSTAL_OUT_PATH/out_adp.out
= "exécute crystal, et écrit la sortie dans le fichier out_adp.out, que tu mettras 
dans le dossier CRYSTAL_OUT_PATH"



##########################################################
2 - slurm_all
##########################################################

"""
first_job=$(sbatch slurm_scf_config.sh)
echo "first job submitted" 

second_job=$(sbatch --dependency=afterok:${first_job##* } slurm_rdm_config.sh)
"""

Ca, ca dit "en premier, tu lances slurm_scf_config.sh. Et SEULEMENT quand il 
est fini, tu lances slurm_rdm_config.sh."

On a besoin de ça parce que si on se contente d'écrire sbatch machin sbatch machin,
il les lance tous sans attendre que les précédents se terminent, et on a un pbm



##########################################################
2 - slurm_mk_dir
##########################################################

Quand on peut passer par python, ne pas hésiter à le faire !!
Coder en shell est assez horrible. Pour ça, écrire un fichier 
python, et écrire simplement dans le fichier slurm :

python3 /gpfs/workdir/benoisa/CRYSTAL/Multiple_Inputs/mk_dir.py



##########################################################
3 : pour une série de configuration (partie moins simple)
##########################################################

Pour écrire la suite, on regarde le fichier slurm_scf_config.sh

"""
for entry in `ls /gpfs/workdir/benoisa/CRYSTAL/Multiple_Inputs/Inputs/scf`; do
"""
ls liste tous les fichiers dans le dossier .../scf : entry est la variable qui contient
le nom du fichier.

"""
echo
	IFS='_'
	read -ra ARR <<< "$entry"
	k=$((ARR[1]))
	IFS=' '
	echo $entry
"""
Le souci, c'est que quand on fait ça, les fichiers arrivent pas forcément dans l'ordre :
c'est problématique si on veut savoir qui produit quel résultat.
L'idée est donc de lire le numéro dans le nom du fichier pour trouver le bon dossier 
lorsqu'on va écrire les résultats.
ARR est donc en gros la chaîne de caractère qui va recevoir le nom du fichier 
sous forme de chaîne de caractère ($entry désigne la valeur de la variable entry,
et "$entry" convertit ça en chaîne de caractère). 
Le "IFS" désigne le symbole "_" dans le nom du fichier : sans ça, lorsqu'on écrit seulement 
la ligne "read -ra ARR <<< "$entry"", il transforme automatiquement les _ en espace (la galère
pour comprendre ça au début...). Ensuite on le remet comme avant avec " IFS=' ' ".
Une fois la chaîne de caractère bien formatée, on n'a plus qu'à récupérer la valeur de k
(= le numéro de la configuration)
echo $entry c'est juste pour voir où on en est ensuite.

"""
INPUT_CRYSTAL=/gpfs/workdir/benoisa/CRYSTAL/Multiple_Inputs/Inputs/scf/$entry
CRYSTAL_OUT_PATH=/gpfs/workdir/benoisa/CRYSTAL/Multiple_Inputs/Output/configuration_$k
"""
On déclare simplement le fichier input et le dossier d'output

"""
cd /gpfs/workdir/benoisa/CRYSTAL/Multiple_Inputs/Output/configuration_$k
"""
Il faut savoir que tous les fichiers intermédiaires, de calcul, ou juste de log
sont crées dans le répertoire de travail.
Conséquence : si on lance bêtement tous les calculs dans le répertoire de travail,
toutes les fonctions d'onde vont se retrouver au même endroit : si on a de la chance
et qu'elles s'écrasent aps mutuellement, elles seront a minima mélangées et donc 
la suite des calculs sera impossible. Pour ça, on change de répertoire, et on va
dans le répertoire cible.


Et après on lance le calcul

# dans le fichier slurm_adp.sh à présent :
"""
if ! [ -d $path_adp_out ]; then
	mkdir $path_adp_out
fi
"""
Ca, ca dit "si il n'existe pas de dossier Output/adp_out, alors crées en un"



##########################################################
4- recommendations générales pour coder en shell
##########################################################

- Ecrire STRICTEMENT ce qu'on voit sur google quand on a un pbm. 
Les moindres espaces ont de l'importance. Exemple dans slurm_all_config :
${first_job##* } MARCHE
${first_job##*} NE MARCHE PAS

- quand on copie depuis google directement, faire tout de même attention à 
l'indentation :
* si on a fait l'indentation avec TAB ça marche
* si on a fait l'indentation avec plusieurs espaces, ça marche pas

- quand on code en shell, Google est (ton seul) ami

- Je me demande si ce que j'ai fait en shell ne peut pas être fait en python 
directement. Avec la librarie os par exemple (beaucoup plus simple à manipuler)

exemple (fonctionne, c'est sur): 
import os
os.system('sbatch slurm.sh')
pour lancer le slurm slurm.sh directement depuis un fichier python

exemple (à tester) :
import os
écrire toutes les lignes dans les slurms avec :
os.system('ligne') pour recréer la série de commande.
Si ça, ça marche, tout peut se faire en python et les galères par lesquelles je suis 
passé pour coder en shell n'étaient pas nécessaires.





