import os


protected = ['fort.25', 'outRDM.out', 'out_scf.out', 'ADP.DAT', 'CO_input_adp.d12']

def remove_heavy_files():
    for i in range(11, 74):
        os.chdir('.\configuration_' + str(i))
        for f in os.listdir('.'):
            if f not in protected and 'fort' in f:
                string =  f
                os.system('del ' + string)
        os.chdir('..')
    os.system('cd')
    return

def copy_fort25():
    for i in range(11, 74):
        os.chdir('.\configuration_' + str(i))
        os.system('copy "fort.25" "../../Fort25/fort25_configuration_' + str(i) + '.25"')
        os.chdir('..')
    return

def remove_heavy_files_2():
    for f in os.listdir('.'):
        if f not in protected and ('fort' in f or 'opta' in f):
            string =  f
            os.system('del ' + string)
    return

# copy_fort25()

os.chdir('./CO2_vf_xfac/CO2_vf/CO2_vf')
remove_heavy_files_2()

print("done")


