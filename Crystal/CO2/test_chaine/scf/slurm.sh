#!/bin/bash

#SBATCH --job-name=XTAL_test
#SBATCH --partition=cpu_short

#SBATCH --time=00:10:00 
#SBATCH --ntasks=10 
#SBATCH --cpus-per-task=10
#SBATCH --exclusive
#SBATCH --mail-user=anthony.benois@student-cs.fr
#SBATCH --mail-type=ALL
#SBATCH --mem=10GB
# Load the same modules as environment configuration

module purge 
module load intel-parallel-studio/cluster.2020.2/intel-20.0.2

CRYSTAL_BIN_PATH=/gpfs/softs/softwares/crystal14/1.0.4/bin

CRYSTAL_OUT_PATH=/gpfs/workdir/benoisa/CRYSTAL/test_chaine/scf

#######  SCF COMPUTATION ############

INPUT_CRYSTAL=/gpfs/workdir/benoisa/CRYSTAL/test_chaine/scf/CO2_input_scf_ex.d12

ln -sf $INPUT_CRYSTAL INPUT

id
srun $CRYSTAL_BIN_PATH/Pcrystal 2> $CRYSTAL_OUT_PATH/co2_scf.out

#######  PROPERTIES COMPUTATION ############

##INPUT_PROPERTIES=OUT.d3 
##INPUT_PROPERTIES=Input_XFACprop.d12

##ln -sf $INPUT_PROPERTIES INPUT
##srun $CRYSTAL_BIN_PATH/Pproperties 2> $CRYSTAL_OUT_PATH/propOUT.out

##rm -f KRED.DAT
##$CRYSTAL_OUT_PATH/Cry_out.exe

