#!/bin/bash

#SBATCH --job-name=XTAL_test
#SBATCH --partition=cpu_long

#SBATCH --time=07:00:00 
#SBATCH --ntasks=10 
#SBATCH --cpus-per-task=10
#SBATCH --exclusive
#SBATCH --mail-user=anthony.benois@student-cs.fr
#SBATCH --mail-type=ALL
#SBATCH --mem=10GB
# Load the same modules as environment configuration

module purge 
module load intel-parallel-studio/cluster.2020.2/intel-20.0.2

CRYSTAL_BIN_PATH=/gpfs/softs/softwares/crystal14/1.0.4/bin


####### get files #######

### Working Directory : Multiple_Inputs ###

declare -i k=0

for entry in `ls /gpfs/workdir/benoisa/CRYSTAL/Multiple_Inputs/Inputs/scf`; do
	
	echo
	IFS='_'
	read -ra ARR <<< "$entry"
	k=$((ARR[1]))
	IFS=' '
	echo $entry
	
	INPUT_CRYSTAL=/gpfs/workdir/benoisa/CRYSTAL/Multiple_Inputs/Inputs/scf/$entry
	CRYSTAL_OUT_PATH=/gpfs/workdir/benoisa/CRYSTAL/Multiple_Inputs/Output/configuration_$k
	
	cd /gpfs/workdir/benoisa/CRYSTAL/Multiple_Inputs/Output/configuration_$k
	
	ln -sf $INPUT_CRYSTAL INPUT
	id
	srun $CRYSTAL_BIN_PATH/Pcrystal 2> $CRYSTAL_OUT_PATH/out_scf.out
	
	k=$((k+1))
	
done

## back to Working dir ###
cd ../../.






