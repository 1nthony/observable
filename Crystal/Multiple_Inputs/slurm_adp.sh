#!/bin/bash

#SBATCH --job-name=XTAL_test
#SBATCH --partition=cpu_short

#SBATCH --time=01:00:00 
#SBATCH --ntasks=80 
##SBATCH --cpus-per-task=80
#SBATCH --exclusive
#SBATCH --mail-user=anthony.benois@student-cs.fr
#SBATCH --mail-type=ALL
#SBATCH --mem=40GB
# Load the same modules as environment configuration

module purge 
module load intel-parallel-studio/cluster.2020.2/intel-20.0.2

CRYSTAL_BIN_PATH=/gpfs/softs/softwares/crystal14/1.0.4/bin


####### get files #######

### Working Directory : Multiple_Inputs ###


for entry in `ls /gpfs/workdir/benoisa/CRYSTAL/Multiple_Inputs/Inputs/adp`; do

	path_adp_out=/gpfs/workdir/benoisa/CRYSTAL/Multiple_Inputs/Output/adp_out
	
	INPUT_CRYSTAL=/gpfs/workdir/benoisa/CRYSTAL/Multiple_Inputs/Inputs/adp/$entry
	CRYSTAL_OUT_PATH=$path_adp_out
	
	if ! [ -d $path_adp_out ]; then
		mkdir $path_adp_out
	fi
	
	cd $path_adp_out
	
	ln -sf $INPUT_CRYSTAL INPUT
	id
	srun $CRYSTAL_BIN_PATH/Pcrystal 2> $CRYSTAL_OUT_PATH/out_adp.out
	
done

## back to Working dir ###
cd ../../.






