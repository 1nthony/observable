#!/bin/bash

#SBATCH --job-name=XTAL_test
#SBATCH --partition=cpu_short

#SBATCH --time=01:00:00 
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --exclusive
#SBATCH --mail-user=anthony.benois@student-cs.fr
#SBATCH --mail-type=ALL
#SBATCH --mem=10GB
# Load the same modules as environment configuration

module purge 


first_job=$(sbatch slurm_scf_config.sh)
echo "first job submitted" 

second_job=$(sbatch --dependency=afterok:${first_job##* } slurm_rdm_config.sh)
echo "second job submitted" 

third_job=$(sbatch --dependency=afterok:${second_job##* } slurm_get_fort25.sh)
echo "third job submitted" 


