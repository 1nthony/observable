#!/bin/bash

#SBATCH --job-name=XTAL_test
#SBATCH --partition=cpu_short

#SBATCH --time=00:10:00 
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --exclusive
#SBATCH --mail-user=anthony.benois@student-cs.fr
#SBATCH --mail-type=ALL
#SBATCH --mem=10GB
# Load the same modules as environment configuration

module purge 

cd /gpfs/workdir/benoisa/CRYSTAL/Multiple_Inputs

declare -i k=0
for entry in `ls /gpfs/workdir/benoisa/CRYSTAL/Multiple_Inputs/Output`; do

	echo
	IFS='_'
	read -ra ARR <<< "$entry"
	k=$((ARR[1]))
	IFS=' '
	echo $entry
	
	
	cp /gpfs/workdir/benoisa/CRYSTAL/Multiple_Inputs/Output/$entry/fort.25 /gpfs/workdir/benoisa/CRYSTAL/Multiple_Inputs/Fort25
	mv /gpfs/workdir/benoisa/CRYSTAL/Multiple_Inputs/Fort25/fort.25 /gpfs/workdir/benoisa/CRYSTAL/Multiple_Inputs/Fort25/fort_$k.25
	
	k=$((k+1))
	
done

## back to Working dir ###
cd ../../.


