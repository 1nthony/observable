import os
from os.path import isfile, join


       
def get_files(location):
    """
    Va chercher les fichiers d'input pour toutes les configurations préétablies.
    
    Argument : adp, scf, rdm
    
    Working directory : CRYSTAL/Multiple_Inputs
    """
    
    path = './Inputs/' + location
    files = [f for f in os.listdir(path) if isfile(join(path, f))]
    return files

def make_dir():
    """
    Créé les dossiers configuration_i dans lesquels on fera les calculs
    Créé aussi un fichier texte listant tous les fichiers croisés

    Attention, l'ordre indiqué par le fichier texte n'est pas forcément le bon
    
    Working directory : CRYSTAL/Multiple_Inputs
    """
    
    ## Ecriture du fichier texte ##
    with open("./liste_fichiers_input.txt",'w',encoding = 'utf-8') as f:
        adp_files = get_files("adp")
        scf_files = get_files("scf")
        rdm_files = get_files("rdm")
        for file in adp_files:
            f.write(file + "\n")
        f.write("\n")
        for file in scf_files:
            f.write(file + "\n")
        f.write("\n")
        for file in rdm_files:
            f.write(file + "\n")
    
    ## Création des dossiers ##
    nb_config = len(scf_files)

    for i in range(nb_config):
        if not os.path.exists('./Output/configuration_' + str(i+1)):
            os.makedirs('./Output/configuration_' + str(i+1))


make_dir()


        

